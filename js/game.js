/// JavaScript

// Spouštěcí sekvence (TODO)
/*
    1. Získat canvas
    2. Vytvořit objekt hry + předsat hře canvas
    3. Vytvořit fuknce výpisu herních dat pro GIU
    4. Vytvořit event kliknutí pro canvas
    5. Vytvořit rutinu vykreslování
    6. Vygenerovat novou mapu, resetovat hodnoty / načíst mapu a hodnoty
    7. Vytvořit první vlnu, uvést hru vchod. 
*/

const Game = class Game {
    constructor(canvas) {
        this.ctx = canvas.getContext('2d');
        this.height = Math.floor(canvas.clientHeight/brick_size);
        this.width = Math.floor(canvas.clientWidth/brick_size);
        this.matrix = new Array(this.width);
        this.end;
        this.begin;
        this.curve = [[-1,-1]];
        this.walkers = []
        this.printLevel = function(x) {};
        this.printBudget = function(x) {};
            /* STATE
            ** 0 - nezahájená hra
            ** 1 - iniciovaná a běžící hra
            ** 2 - Konec hry
            ** 3 - Ukončená hra
            **/
        this.gameVarialbes = {
            'state': 0,
            'budget': 303,
            'level': 0,
            'lives': 3,
            "speedMultiplier": 1
        }
        this.game_step_delay = 30;
        this.set_game_clock();
    }

    set_game_clock(miliseconds) {
        if (miliseconds != undefined) {
            this.game_step_delay = miliseconds;
        }
        this.stop_game_clock();
        this.interval = setInterval(this.stepOwerlord, this.game_step_delay, this);
    }

    stop_game_clock() {
        if (this.interval) {
            clearInterval(this.interval);
        } 
    }

    generateEmptyMatrix() {
        for (var column = 0; column < this.width; column++) {
            this.matrix[column] = new Array(this.height);
            for (var row = 0; row < this.height; row++) {
                this.matrix[column][row] = new Brick(column, row, this.ctx, this.matrix, this.walkers);
            }
        }
    }

    createWayByCurve(curve) {
        if (curve != undefined) {
            this.curve = curve;
        }
        let end = this.curve[this.curve.length - 1]
        for (var index = 1; index < this.curve.length; index++) {
            var x = this.curve[index-1][0];
            var y = this.curve[index-1][1];
            var dx = Math.sign(this.curve[index][0] - x);
            var dy = Math.sign(this.curve[index][1] - y);
            while ((this.curve[index][0] != x) || (this.curve[index][1] != y)) {
                if (Math.abs(this.curve[index][0] - x) > Math.abs(this.curve[index][1] - y)) {
                    x += dx
                } else {
                    y += dy
                }
                this.setOn(x,y, Way);
            }
        }
        this.matrix[this.curve[0][0]][this.curve[0][1]] = new Begin(this.curve[0][0],this.curve[0][1], this.ctx, this.matrix, this.walkers, this.curve, this.gameVarialbes);
        this.begin = this.matrix[this.curve[0][0]][this.curve[0][1]];
        this.matrix[end[0]][end[1]] = new End(end[0],end[1], this.ctx, this.matrix, this.walkers, this.gameVarialbes);
        this.end = this.matrix[end[0]][end[1]];
    }

    generateCurve(targetLength) {
        var length = 1;
        var curve = [
            [
                1 + Math.round(Math.random() * (this.width - 3)),
                1 + Math.round(Math.random() * (this.height - 3))
            ]
        ];
        var x = curve[0][0];
        var y = curve[0][1];
        var direction = Math.round(Math.random());
        console.log("New Curve");
        while (length < targetLength) {
            if (direction) {
                var nx;
                do {
                    let mx = 2 + Math.round(Math.random() * (this.width - 2));
                    mx *= Math.round(Math.random())*2 - 1;
                    nx = x + mx;
                } while ((nx < 1) || (nx > this.width - 2))
                length += Math.abs(x - nx);
                x = nx;
                curve.push([x,y]) 
            } else {
                var ny;
                do {
                    let my = 2 + Math.round(Math.random() * (this.height - 2));
                    my *= Math.round(Math.random())*2 - 1;
                    ny = y + my;
                } while ((ny < 1) || (ny > this.height - 2))
                length += Math.abs(y - ny);
                y = ny;
                curve.push([x,y]) 
            }
            direction = !direction;
        }
        return curve;
    }


    showCurve() {
        this.ctx.beginPath();                    
        this.ctx.moveTo(this.curve[0][0] * brick_size ,this.curve[0][1] * brick_size);
        for (let i = 0; i < this.curve.length; i++) {
            this.ctx.lineTo((this.curve[i][0]+0.5) * brick_size, (this.curve[i][1]+0.5) * brick_size);
        }
        this.ctx.lineWidth = 4;
        this.ctx.strokeStyle = '#ff0000';
        this.ctx.stroke();
    }


    setOn(x, y, object) {
        this.matrix[x][y] = new object(x,y,this.ctx, this.matrix, this.walkers);
    }

    setOnGun(x,y,object) {
        if (this.matrix[x][y].type == 'brick') {
            this.setOn(x,y,object);
            return true;
        }
        return false;
    }

    render() {
        for (var column = 0; column < this.width; column++) {
            for (var row = 0; row < this.height; row++) {
                this.matrix[column][row].render_main();
            }
        }
        for (let walker = 0; walker < this.walkers.length; walker++) {
            this.walkers[walker].render();
        }
        for (var column = 0; column < this.width; column++) {
            for (var row = 0; row < this.height; row++) {
                this.matrix[column][row].render_slave();
            }
        }
        if (this.gameVarialbes.state == 2) {
            this.render_gameOver();
        }
        this.printGunInfo();
    }

    render_gameOver() {
        let img_game = document.getElementById('img_game');
        let img_over = document.getElementById('img_over');
        let pos_x = brick_size * this.width/2 - 200;
        let pos_y = brick_size * this.height/2 - 140;
        this.ctx.drawImage(img_game, pos_x, pos_y);
        this.ctx.drawImage(img_over, pos_x, pos_y+140);
    }

    step() {
        if (this.gameVarialbes.state != 1) return;
        for (var column = 0; column < this.width; column++) {
            for (var row = 0; row < this.height; row++) {
                this.matrix[column][row].step();
            }
        }
        for (let walker = 0; walker < this.walkers.length; walker++) {
            this.walkers[walker].step();
        }
        if (Math.min.apply(Math, this.walkers.map(function(x) {return x.disabled})) == 1) {
            this.round();
        }
        /// Sem to nepatří, ale zatím není kam
        this.printBudget(this.gameVarialbes.budget);
        this.printLives(this.gameVarialbes.lives);
        if (this.gameVarialbes.lives == 0) {
            this.gameVarialbes.state = 2
        }
    }

    round() {
        this.gameVarialbes.level++;
        while (this.walkers.length > 0) {
            this.walkers.pop();
        }
        let enemies = [Runner, Walker, Creeper];
        this.begin.setSpawn(enemies[this.gameVarialbes.level%3], 8 + Math.floor(this.gameVarialbes.level / 2), 20 - this.gameVarialbes.level%15, this.gameVarialbes.level);
        this.printLevel(this.gameVarialbes.level);
    }

    start() {
        // TODO: Předělat, aby byl schopen přijmout uloženou hru, měnit automaticky veikost canvas, restartovat poslední hru, vytvořit novou
        this.generateEmptyMatrix();
        this.createWayByCurve( document.game.generateCurve(this.width*this.height / 10));
        this.gameVarialbes.state = 1;
    }

    stepOwerlord(step_object) {
        if (step_object.gameVarialbes.state > 0) {
            for (let i = 0; i < step_object.gameVarialbes.speedMultiplier; i++) {
                step_object.step();
            }
            step_object.render()
        }
    }

    simpleRestart() {
        this.gameVarialbes = {
            'state': 1,
            'budget': 303,
            'level': 0,
            'lives': 3
        }
        this.generateEmptyMatrix();
        this.createWayByCurve();
        this.round();
    }

    loadSave(jsonData) {
        // TEST DATA
        // document.game.loadSave('{"height":10,"width":10,"curve":[[2,2],[2,6],[6,6],[6,8],[3,8],[3,2]],"level":2,"lives":6,"budget":5000,"towers":[{"x":4,"y":4,"type":"slowdowner"},{"x":5,"y":4,"type":"multilaser"}]}')
        // TODO:
        // * Verifikace dat
        // * Nadstavba pro reset hry při načtení
        let data = JSON.parse(jsonData);
        this.height = data.height;
        this.width = data.width;
        this.curve = data.curve;
        this.gameVarialbes.level = data.level;
        this.gameVarialbes.lives = data.lives;
        this.gameVarialbes.budget = data.budget;
        this.generateEmptyMatrix();
        this.createWayByCurve(data.curve);
        let tower_types = {
            "brick": Brick,
            "way": Way,
            "gun": Gun,
            "singlelaser": SingleLaser,
            "multilaser": MultiLaser,
            "rockettower": RocketTower,
            "slowdowner":  Slowdowner
        }
        data.towers.forEach((tower) => {
            this.setOn(tower.x, tower.y, tower_types[tower.type]);
        })
    }
}

function run() {
    // Ziskam canvas
    var canvas_main = document.getElementById('main_display');
    // Ziskam, zaokrouhlim rozmery canvasu a aplikuji
    var height = brick_size * Math.floor(canvas_main.clientHeight/brick_size);
    var width = brick_size * Math.floor(canvas_main.clientWidth/brick_size);
    canvas_main.height = height;
    canvas_main.width = width;
    canvas_main.style = "height: "+height+"; width: "+width;
    // Vyrvorim novy objekt hry
    document.game = new Game(canvas_main);
    // nastavim aktivni vez
    document.activeGun = SingleLaser;
    // Nastaveni eventu kliknuti na canvas - umisteni nove veze
    canvas_main.addEventListener('click', function(evt) {
        function getMousePos(canvas, evt) {
            var rect = canvas.getBoundingClientRect();
            return {
                x: evt.clientX - rect.left,
                y: evt.clientY - rect.top
            };
        }
        let mousePos = getMousePos(canvas_main, evt);
        let x = Math.round((mousePos.x - brick_size/2)/brick_size);
        let y = Math.round((mousePos.y - brick_size/2)/brick_size);
        if (document.activeGun != null) {
            let example = new document.activeGun();
            if (document.game.gameVarialbes.budget >= example.cost) {
                if (document.game.setOnGun(x, y, document.activeGun)) document.game.gameVarialbes.budget -= example.cost;
            }
            if (document.activeGun == Brick) {
                document.game.setOn(x, y, document.activeGun);
            }
            document.activeGun == null;
        }
    }, false);
    // nastaveni cilu funkci pro vypsani pomocnych dat do GUI
    document.game.printLevel = function(level) {
        document.getElementById('level').innerHTML = level;
    }
    document.game.printBudget = function(budget) {
        document.getElementById('budget').innerHTML = budget + "$";
    }
    document.game.printLives = function(budget) {
        document.getElementById('lives').innerHTML = "♥ ".repeat(budget);
    }
    document.game.printGunInfo = function() {
        let gun = new document.activeGun();
        document.getElementById('gun_info').innerHTML = "<br><i>"+gun.type+"</i><br><b>Power: "+gun.power+"</b><br><b>Range:"+gun.range+"</b><br><b>Cost:"+gun.cost+"</b>"
    }
}

function demo() {
    if (document.game.gameVarialbes.state == 0){
        document.game.start();
        document.game.round();
    }
    if (document.game.gameVarialbes.state == 2){
        document.game.simpleRestart()
    }
}

function new_map() {
    if (document.game.gameVarialbes.state in [0, 2]) {
        document.game.generateEmptyMatrix();
        document.game.createWayByCurve( document.game.generateCurve(document.game.width*document.game.height / 10));
        document.game.render();
    }
}
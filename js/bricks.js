const Brick = class Brick {
    constructor(column, row, display, matrix, walkers) {
        this.x = column;
        this.y = row;
        this.ctx = display;
        this.matrix = matrix;
        this.walkers = walkers;
        this.color = "purple";
        this.type = 'brick';
        this.way = false;
        this.cost = 0;
    }
    render_main() {
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x * brick_size, this.y * brick_size, brick_size, brick_size);
        this.ctx.fillStyle = "black";
        this.ctx.fillRect(this.x * brick_size + 1, this.y * brick_size + 1, brick_size-2, brick_size-2);
    }
    step() {
        ;
    }
    render_slave() {
        ;
    }
    get_center() {
        return [
            ( this.x + 0.5 ) * brick_size,
            ( this.y + 0.5 ) * brick_size
        ]
    }
}

const Way = class Way extends Brick {
    constructor(column, row, display, matrix, walkers) {
        super(column, row, display, matrix, walkers);
        this.color = "white";
        this.type = 'way';
        this.way = true;
    }
    render_main() {
        this.ctx.fillStyle = "black";
        this.ctx.fillRect(this.x * brick_size, this.y * brick_size, brick_size, brick_size);
        this.ctx.fillStyle = this.color;

        if (!this.matrix[this.x-1][this.y].way) {
            this.ctx.fillRect(this.x * brick_size, this.y * brick_size, 2, brick_size);
        }
        if (!this.matrix[this.x+1][this.y].way) {
            this.ctx.fillRect((this.x + 1) * brick_size - 2, this.y * brick_size, 2, brick_size);
        }
        if (!this.matrix[this.x][this.y-1].way) {
            this.ctx.fillRect(this.x * brick_size, this.y * brick_size, brick_size, 2);
        }
        if (!this.matrix[this.x][this.y+1].way) {
            this.ctx.fillRect(this.x * brick_size, (this.y + 1) * brick_size - 2, brick_size, 2);
        }
    }
}

const Gun = class Gun extends Brick {
    constructor(column, row, display, matrix, walkers) {
        super(column, row, display, matrix, walkers);
        this.color = "white";
        this.type = 'gun';
        this.power = 1;
        this.range = 1;
        this.cost = 111;
    }
}

const SingleLaser = class SingleLaser extends Gun {
    constructor(column, row, display, matrix, walkers) {
        super(column, row, display, matrix, walkers);
        this.color = "orange";
        this.type = 'singlelaser';
        this.power = 4;
        this.range = 3;
        this.cost = 98;
    }
    render_main() {
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x * brick_size, this.y * brick_size, brick_size, brick_size);
        this.ctx.fillStyle = "black";
        this.ctx.fillRect(this.x * brick_size + 1, this.y * brick_size + 1, brick_size-2, brick_size-2);
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect((this.x + 0.0) * brick_size + 4, (this.y + 0.0) * brick_size + 4, brick_size/2 - 6, brick_size/2 -6);
        this.ctx.fillRect((this.x + 0.5) * brick_size + 2, (this.y + 0.0) * brick_size + 4, brick_size/2 - 6, brick_size/2 -6);
        this.ctx.fillRect((this.x + 0.0) * brick_size + 4, (this.y + 0.5) * brick_size + 2, brick_size/2 - 6, brick_size/2 -6);
        this.ctx.fillRect((this.x + 0.5) * brick_size + 2, (this.y + 0.5) * brick_size + 2, brick_size/2 - 6, brick_size/2 -6);
    }
    render_slave() {
        this.ctx.beginPath();
        for (let index = 0; index < this.walkers.length; index++) {
            if (this.walkers[index].disabled) continue;
            let pos = this.walkers[index].get_center();
            if ((pos[0] - this.x*brick_size)**2 + (pos[1] - this.y*brick_size)**2 < (brick_size*this.range)**2) {
                this.ctx.moveTo((this.x + 0.5) * brick_size, (this.y + 0.5) * brick_size);
                this.ctx.lineTo(pos[0], pos[1]);
                break;
            }
        }
        this.ctx.lineWidth = 3;
        this.ctx.strokeStyle = this.color;
        this.ctx.stroke();
    }
    step() {
        for (let index = 0; index < this.walkers.length; index++) {
            if (this.walkers[index].disabled) continue;
            let pos = this.walkers[index].get_center();
            if ((pos[0] - this.x*brick_size)**2 + (pos[1] - this.y*brick_size)**2 < (brick_size*this.range)**2) {
                this.walkers[index].damage(this.power);
                break;
            }
        }
    }
}

const MultiLaser = class MultiLaser extends Gun {
    constructor(column, row, display, matrix, walkers) {
        super(column, row, display, matrix, walkers);
        this.color = "red";
        this.type = 'multilaser';
        this.power = 1;
        this.range = 5;
        this.cost = 140;
    }
    render_main() {
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x * brick_size, this.y * brick_size, brick_size, brick_size);
        this.ctx.fillStyle = "black";
        this.ctx.fillRect(this.x * brick_size + 1, this.y * brick_size + 1, brick_size-2, brick_size-2);
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect((this.x + 0.0) * brick_size + 4, (this.y + 0.0) * brick_size + 4, brick_size/2 - 6, brick_size/2 -6);
        this.ctx.fillRect((this.x + 0.5) * brick_size + 2, (this.y + 0.0) * brick_size + 4, brick_size/2 - 6, brick_size/2 -6);
        this.ctx.fillRect((this.x + 0.0) * brick_size + 4, (this.y + 0.5) * brick_size + 2, brick_size/2 - 6, brick_size/2 -6);
        this.ctx.fillRect((this.x + 0.5) * brick_size + 2, (this.y + 0.5) * brick_size + 2, brick_size/2 - 6, brick_size/2 -6);
    }
    render_slave() {
        this.ctx.beginPath();
        for (let index = 0; index < this.walkers.length; index++) {
            if (this.walkers[index].disabled) continue;
            let pos = this.walkers[index].get_center();
            if ((pos[0] - this.x*brick_size)**2 + (pos[1] - this.y*brick_size)**2 < (brick_size*this.range)**2) {
                this.ctx.moveTo((this.x + 0.5) * brick_size, (this.y + 0.5) * brick_size);
                this.ctx.lineTo(pos[0], pos[1]);
            }
        }
        this.ctx.lineWidth = 1;
        this.ctx.strokeStyle = this.color;
        this.ctx.stroke();
    }
    step() {
        for (let index = 0; index < this.walkers.length; index++) {
            if (this.walkers[index].disabled) continue;
            let pos = this.walkers[index].get_center();
            if ((pos[0] - this.x*brick_size)**2 + (pos[1] - this.y*brick_size)**2 < (brick_size*this.range)**2) {
                this.walkers[index].damage(this.power);
            }
        }
    }
}

const Rocket = class Rocket {
    constructor(display, column,row,walkers, damage) {
        this.ctx = display;
        this.column = column;
        this.row = row;
        this.x = column * brick_size;
        this.y = row * brick_size;
        this.walkers = walkers;
        this.active = false;
        this.size = 8;
        this.target = -1;
        this.speed = 7;
        this.damage = damage;
    }
    render() {
        if (this.active) {
            this.ctx.fillRect(this.x, this.y, this.size, this.size)
    }
        }
    step() {
        if (!this.active) return;
        if (!this.walkers[this.target]) {
            this.active = false;
            return;
        }
        let rx = this.x;
        let ry = this.y;
        let tx = this.walkers[this.target].position[0];
        let ty = this.walkers[this.target].position[1];
        if (Math.abs(rx - tx) > this.speed / 2) {
            this.x -= Math.sign(rx - tx) * this.speed;
        }
        if (Math.abs(ry - ty) > this.speed / 2) {
            this.y -= Math.sign(ry - ty) * this.speed;
        }

        if ((Math.abs(rx - tx) < this.speed) && (Math.abs(ry - ty) < this.speed)) {
            this.active = false;
            this.walkers[this.target].damage(this.damage)
        }
    }
    launch(walkerIndex) {
        this.x = this.column * brick_size;
        this.y = this.row * brick_size;
        this.target = walkerIndex;
        this.active = true;
    }
}

const RocketTower = class RocketTower extends Gun {
    constructor(column, row, display, matrix, walkers) {
        super(column, row, display, matrix, walkers);
        this.color = "lime";
        this.type = 'rockettower';
        this.power = 230;
        this.showSpeed = 12;
        this.range = 2;
        this.cutdown = 40;
        this.clock = 20;
        this.maxRockets = 10;
        this.cost=120;
        this.shots = new Array(this.maxRockets);
        for (let i = 0; i < this.shots.length; i++){
            this.shots[i] = new Rocket(this.ctx, column, row, this.walkers, this.power);
        }
    }
    render_main() {
        let fill = this.clock / this.cutdown;
        const neutral_color = "#103010";
        if (fill > 1) { fill = 1};
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x * brick_size, this.y * brick_size, brick_size, brick_size);
        this.ctx.fillStyle = "black";
        this.ctx.fillRect(this.x * brick_size + 1, this.y * brick_size + 1, brick_size-2, brick_size-2);
        if (fill >= 0.25) {this.ctx.fillStyle = this.color;} else {this.ctx.fillStyle = neutral_color;}
        this.ctx.fillRect((this.x + 0.0) * brick_size +  2, (this.y + 0.0) * brick_size +  2,               4,               4);
        if (fill >= 0.50) {this.ctx.fillStyle = this.color;} else {this.ctx.fillStyle = neutral_color;}
        this.ctx.fillRect((this.x + 1.0) * brick_size -  6, (this.y + 0.0) * brick_size +  2,               4,               4);
        if (fill >= 0.75) {this.ctx.fillStyle = this.color;} else {this.ctx.fillStyle = neutral_color;}
        this.ctx.fillRect((this.x + 0.0) * brick_size +  2, (this.y + 1.0) * brick_size -  6,               4,               4);
        if (fill >= 1) {this.ctx.fillStyle = this.color;} else {this.ctx.fillStyle = neutral_color;}
        this.ctx.fillRect((this.x + 1.0) * brick_size -  6, (this.y + 1.0) * brick_size -  6,               4,               4);
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect((this.x + 0.0) * brick_size +  6, (this.y + 0.0) * brick_size +  6, brick_size - 12, brick_size - 12);
        this.ctx.fillStyle = "black";
        this.ctx.fillRect((this.x + 0.0) * brick_size + 12, (this.y + 0.0) * brick_size + 12, brick_size - 24, brick_size - 24);
    }
    render_slave() {
        this.ctx.fillStyle = this.color;
        for (let x = 0; x < this.shots.length; x++) {
            this.shots[x].render();
        }
    }

    step() {
        this.clock++;
        if (this.clock >= this.cutdown) {
            this.clock = this.cutdown;
            for (let index = 0; index < this.walkers.length; index++) {
                if (this.walkers[index].disabled) continue;
                if (this.walkers[index].is_in_range(this)) {
                    if (this.shot(index)) {
                        this.clock = Math.round(Math.random()*4);
                    } else {
                        this.clock -= 5;
                    }
                    break;
                }
            }
        }
        for (let x = 0; x < this.shots.length; x++) {
            this.shots[x].step();
        }
    }

    shot(walkerIndex) {
        for (let index = 0; index < this.maxRockets; index++) {
            if (!this.shots[index].active) {
                this.shots[index].launch(walkerIndex);
                return true;
            }
        }
        return false;
    }
}

const Slowdowner = class Slowdowner extends Gun {
    constructor(column, row, display, matrix, walkers) {
        super(column, row, display, matrix, walkers);
        this.color = "#00aeff";
        this.type = 'slowdowner';
        this.power = 6;
        this.range = 4;
        this.time = 150;
        this.cutdown = 200;
        this.clock = 0;
        this.cost=170;
    }
    render_main() {
        let fill = this.clock / this.cutdown;
        const neutral_color = "#101030";
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x * brick_size, this.y * brick_size, brick_size, brick_size);
        if (this.clock == 0) {return;};
        if (fill > 1) { fill = 1};
        this.ctx.fillStyle = "black";
        this.ctx.fillRect(this.x * brick_size + 1, this.y * brick_size + 1, brick_size-2, brick_size-2);

        for (let row = 0; row < 3; row++) {
            for (let column = 0; column < 3; column++) {
                if (fill*9 >= row + column*3 + 1) {
                    this.ctx.fillStyle = this.color;
                } else {
                    this.ctx.fillStyle = neutral_color;
                }
                this.ctx.fillRect(this.x * brick_size + 3 + row*10, this.y * brick_size + 3 + column*10, 6, 6);
            }
        }
    }
    render_slave() {
        if (this.clock > 7) return;
        this.ctx.strokeStyle = this.color;
        let pos = this.get_center();
        //for (let i = 10; i > 3; i--) {            
        let i = this.clock + 3;
        this.ctx.beginPath();
        this.ctx.lineWidth = Math.ceil(i/7);
        this.ctx.arc(pos[0], pos[1], this.range*brick_size*i/10, 0, 2*Math.PI);
        this.ctx.stroke();
        //}
    }
    step() {
        this.clock++;
        if (this.clock >= this.cutdown) {
            let fired = false;
            let close_range_ids = [];
            let long_range_ids = [];
            for (let index = 0; index < this.walkers.length; index++) {
                if (this.walkers[index].disabled || this.walkers[index].effects_by("slowdown")) continue;
                if (this.walkers[index].is_in_range(this, this.range/2)) {
                    close_range_ids.push(index);
                }
                if (this.walkers[index].is_in_range(this)) {
                    long_range_ids.push(index);
                }
            }

            if (close_range_ids.length >= 1 || long_range_ids.length >= 3) {
                let ids = close_range_ids.concat(long_range_ids);
                for (let index in ids) {
                    this.walkers[ids[index]].apply_effect("slowdown", this.power, this.time);
                }
                fired = true;
            }
            if (fired) {this.clock = 0;}
        }
    }
}

const SpecialWay = class SpecialWay extends Way{
    render_main() {
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x * brick_size, this.y * brick_size, brick_size, brick_size);
        this.ctx.fillStyle = "black";
        this.ctx.fillRect(this.x * brick_size + 2, this.y * brick_size + 2, brick_size - 4, brick_size - 4);
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x * brick_size + 4, this.y * brick_size + 4, brick_size - 8, brick_size - 8);
    }
}

const Begin = class Begin extends SpecialWay {
    constructor(column, row, display, matrix, walkers, curve, gameVarialbes) {
        super(column, row, display, matrix, walkers);
        this.color = "lime";
        this.type = 'begin';
        this.way = true;
        this.clock = 0;
        this.timeout = 0;
        this.curve = curve;
        this.gameVarialbes = gameVarialbes;
    }

    setSpawn(walker, number, timeout, level) {
        this.number = number;
        this.timeout = timeout;
        this.clock = 0;
        this.walker = walker;
        this.spawnLevel = level;
    }

    step() {
        if (this.number > 0) {
            if (this.clock > this.timeout) {
                this.clock = 0;
                this.walkers.push(new this.walker(this.curve, this.ctx, this.gameVarialbes));
                this.number--;
            }
            this.clock++;
        }
    }
}

const End = class End extends SpecialWay {
    constructor(column, row, display, matrix, walkers, gameVarialbes) {
        super(column, row, display, matrix, walkers);
        this.color = "red";
        this.type = 'end';
        this.way = true;
        this.gameVarialbes = gameVarialbes;
    }
}

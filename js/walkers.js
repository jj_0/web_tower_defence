const Walker = class Walker {
    constructor(curve, display, gameVarialbes) {
        this.curve = curve;
        this.speed = 4;
        this.point = 1;
        this.position = [-1, -1];
        this.ctx = display;
        this.size = 10;
        this.moveToPosition(curve[0]);
        this.target = [-1, -1]
        this.target[0] = this.position[0];
        this.target[1] = this.position[1];
        this.disabled = false;
        this.color = "white";
        this.price = 4;
        this.gameVarialbes = gameVarialbes;
        this.setHealth(50 + (gameVarialbes['level']*10));
        this.effects = {
            "slowdown": {
                power: 0,
                time: 0
            }
        };
    }

    setHealth(value) {
        this.health = value;
        this.health_full = this.health;
    }

    moveToPosition(target) {
        this.position[0] = target[0] * brick_size + ((brick_size - this.size)/2);
        this.position[1] = target[1] * brick_size + ((brick_size - this.size)/2);
    }

    render() {
        this.render_body();
        if (!this.disabled) this.render_health();
    }

    render_body() {
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.position[0], this.position[1], this.size, this.size);
    }

    render_health() {
        let health_size = (this.size + 2) * this.health / this.health_full;
        this.ctx.fillStyle = "lime";
        this.ctx.fillRect(this.position[0] - 1, this.position[1] - 4, this.size + 2, 3);
        this.ctx.fillStyle = "red";
        this.ctx.fillRect(this.position[0] + health_size -1, this.position[1] - 4, this.size + 2 - health_size, 3);
    }

    damage(power) {
        this.health -= power;
        if (this.health < 0) {
            this.disabled = true;
            this.gameVarialbes['budget']+=this.price;
            this.color = "#151515";
        }
    }

    step() {
        if (this.disabled) return;
        let speed = this.speed;
        if (this.effects.slowdown.time > 0 && this.effects.slowdown.power > 0) {
            speed = this.speed / this.effects.slowdown.power;
            this.effects.slowdown.time--;
        }
        for (let i = 0; i < 2; i++) {
            this.position[i] += Math.sign(this.target[i] - this.position[i]) * speed;
        }
        if ((Math.abs(this.position[0] - this.target[0]) < speed) && (Math.abs(this.position[1] - this.target[1]) < speed)) {
            this.moveToPosition(this.curve[this.point-1]);
            if (this.point == this.curve.length) {
                console.log("Someone in house.");
                this.disabled = true;
                this.gameVarialbes.lives--;
                return;
            }
            this.target[0] = this.curve[this.point][0] * brick_size + ((brick_size - this.size)/2);
            this.target[1] = this.curve[this.point][1] * brick_size + ((brick_size - this.size)/2);
            this.point++;
        }
    }

    get_center() {
        return [this.position[0] + this.size/2, this.position[1] + this.size/2]
    }

    is_in_range(brick, distance) {
        if (distance == undefined) {
            distance = brick.range;
        }
        let me = this.get_center();
        let him = brick.get_center();
        return Math.pow(me[0]-him[0], 2) + Math.pow(me[1]-him[1], 2) <= Math.pow(distance*brick_size, 2);
    }

    isInEnd() {
        return (
            (this.position[0]+(this.size/2) == (this.curve[this.curve.length - 1][0] + 0.5) * brick_size)
            &&
            (this.position[1]+(this.size/2) == (this.curve[this.curve.length - 1][1] + 0.5) * brick_size)
            );
    }

    disable() {
        this.disabled = true;
    }

    apply_effect(type, power, time) {
        if (type in this.effects) {
            if (this.effects[type].time == 0) {
                this.effects[type].time = time;
                this.effects[type].power = power;
                return;
            }
            /* TODO:
             * Add some logic to chose best combination of new and working efect
            */
            if (this.effects[type].power < power) {
                this.effects[type].power = power;
            }
            if (this.effects[type].time < time) {
                this.effects[type].time = time;
            }
        }
    }
    effects_by(type) {
        if (type in this.effects) {
            return this.effects[type].time > 5
        }
        return false;
    }
}

const Runner = class Runner extends Walker {
    constructor (curve, display, gameVarialbes) {
        super(curve, display, gameVarialbes);
        this.speed = 6 + Math.round(gameVarialbes.level/20);
        this.color = "yellow";
        this.setHealth(65 + (gameVarialbes.level*4));
    }
}

const Creeper = class Creeper extends Walker {
    constructor (curve, display, gameVarialbes) {
        super(curve, display, gameVarialbes);
        this.speed = 2;
        this.color = "wheat";
        this.setHealth(220 + (gameVarialbes.level*10));
    }
}
